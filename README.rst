#################################################
Embedded C++ Platform Project AVR8 target support
#################################################

This project is part of the embedded C++ Platform Project. It
provides build support for Microchip AVR8 targets.

.. _`glibc`: https://www.gnu.org/software/libc/

The project also provides a selection of c++ header files taken
from `glibc`_ to allow use of some C++ STL functions.

License
=======

Files contained in subfolder `glibcxx` are licensed
as declared in the file's header.

For Files in subfolder `src` if not otherwise declared 
in the file's header are published under the terms of
GNU General Public License version 3 with linking 
exception.

Copyright
=========

Copyright for source code files contained in subfolder `glibcxx` is stated 
in the file's header files.

Copyright for source code files in subfolder `src` if not otherwise stated 
in the file's header is Copyright (c) 2023 Andreas Messer <andi@bastelmap.de>


References
==========

.. target-notes::