/*
 * AVR(.hpp
 *
 *  Created on: 11.04.2015
 *      Author: andi
 */

#ifndef ECPP_TARGET_AVR8_HPP_
#define ECPP_TARGET_AVR8_HPP_

#include "ecpp/HAL/AVR8/IOPort.hpp"
#include "ecpp/HAL/AVR8/Memory.hpp"
#include "ecpp/HAL/AVR8/SPI.hpp"
#include "ecpp/HAL/AVR8/TWI.hpp"
#include "ecpp/HAL/AVR8/USI.hpp"

#endif /* ECPP_TARGET_AVR8_HPP_ */
